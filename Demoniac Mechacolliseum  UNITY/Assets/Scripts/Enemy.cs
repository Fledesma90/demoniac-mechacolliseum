﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

using System;

public class Enemy : MonoBehaviour
{
    public float health;

    public float speed;

    [HideInInspector]

    public Transform player;

    public NavMeshAgent navAgent;

    public GameObject bullet;

    public Transform shootPoint;
    //mide la frecuencia de disparo.
    public float firingRate;

    public ParticleSystem deathParticles;

    public enum enemyTypes {Enemy1, Enemy2, Boss};

    public enemyTypes type;

    private bool reloading;

    private int bulletsShot;

    public int bulletEnemy2Boss;

    public GameObject[] dropablesEnemies;

    public GameObject[] dropablesBosses;

    public static Action OnDead;

    // Start is called before the first frame update
    void Start()
    {

        InvokeRepeating("PlayerSeeking",0f,0.5f);

        player = GameObject.FindGameObjectWithTag("Player").transform;

        navAgent.updateRotation = false;

        InvokeRepeating("Shoot", 0f, firingRate);
    }

    // Update is called once per frame
    void Update() {
      

    }

    private void LateUpdate() {
        //que haga la rotacion si se esta moviendo
        if (navAgent.velocity.sqrMagnitude>Mathf.Epsilon)
        {
            transform.rotation = Quaternion.LookRotation(navAgent.velocity.normalized);
        }

        if (Vector3.Distance(transform.position, player.position)<=navAgent.stoppingDistance) {
            Vector3 Lookpos = new Vector3(player.position.x, transform.position.y, player.position.z);
            transform.LookAt(Lookpos);
        }
    }

    public void TakeHealth(float healthAmount) {
        health -= healthAmount;

        if (health<=0) {
            Die();

        }
    }

    private void Die() {
        //Efecto de morir
        Instantiate(deathParticles, transform.position, Quaternion.identity);
        DropItems();
        OnDead?.Invoke();
        //avisamos al game manager que ha muerto un enemigo
        GameManager.instance.EnemyDead();
        Destroy(gameObject);
        
    }
    //Declaramos corrutina de disparo para que los disparos tengan un margen de tiempo.
    void Shoot() {
        if (!reloading) {
            Instantiate(bullet, shootPoint.position, transform.rotation);
            bulletsShot++;
            if (bulletsShot >= bulletEnemy2Boss) {
                
                if (type == enemyTypes.Enemy2 || type== enemyTypes.Boss) {
                    StartCoroutine(ReloadTime());
                }
            }
        }
    }
    IEnumerator ReloadTime() {
        reloading = true;
        yield return new WaitForSeconds(2f);
        reloading = false;
        //reiniciamos las balas del enemigo2 y boss
        bulletsShot = 0;
    }
    void PlayerSeeking() {
        navAgent.SetDestination(player.position);
        
    }

    void DropItems() {
        //si generamos un numero menos o igual que 25 por ciento los enemigos dropean PowerUp
        if (type==enemyTypes.Enemy1 || type==enemyTypes.Enemy2) {
            if (UnityEngine.Random.Range(0, 100) <= 25) {
                Instantiate(dropablesEnemies[UnityEngine.Random.Range(0, dropablesEnemies.Length)], transform.position, Quaternion.identity);

            }
        }
        
        if (type==enemyTypes.Boss) {
            Instantiate(dropablesBosses[UnityEngine.Random.Range(0, dropablesBosses.Length)], transform.position, Quaternion.identity);
        }
    }
}
