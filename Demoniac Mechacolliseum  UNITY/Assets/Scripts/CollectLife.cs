﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectLife : MonoBehaviour
{
    public AudioSource collectSound;

    private void OnTriggerEnter(Collider other) {
        GlobalAchievments.achIloveuCounter += 1;
        collectSound.Play();
        Destroy(gameObject);
    }
}
