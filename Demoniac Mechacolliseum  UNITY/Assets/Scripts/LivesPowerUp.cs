﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LivesPowerUp : MonoBehaviour
{
    public float livesToAdd;

    public GameObject effectParticles;

    private void OnTriggerEnter(Collider other) {
        GlobalAchievments.achIloveuCounter += 1;
    }

    public void TakeLives() {
        Instantiate(effectParticles, transform.position,Quaternion.identity);
        Destroy(gameObject);
    }
    

    
}
