﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//querems serializarlo para guardrlo en texto de tipo binario
[System.Serializable]
public class Stat
    {
    //codigo para identificar la estadistica
    public string code;
    //valor acumulado de estadistica
    public int value;
}
