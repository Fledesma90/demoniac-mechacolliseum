﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

using System.Linq;
public class AchievementManager : MonoBehaviour
{
    #region VARIABLES ############################
    public static Action<string, string> OnAchivementUnlock;
    #endregion

    #region EVENTS #####################
    private void OnEnable() {
        Enemy.OnDead += () => IncreaseStatAndCheckAchievement("kill", 1);
    }

    private void OnDisable() {
        Enemy.OnDead -= () => IncreaseStatAndCheckAchievement("kill", 1);
    }
    #endregion

    #region METHODS ######################

    private void EnemyDead() {
        IncreaseStatAndCheckAchievement("kill", 1);
    }

    /// <summary>
    /// Incrementa con estaditcia y  verifica 
    /// </summary>
    /// <param name="code"></param>
    /// <param name="amount"></param>
    public void IncreaseStatAndCheckAchievement(string code, int amount) {
        //recuperamos de entre las estaditicas la qeenga el code indicado
        Stat stat = DataManager.instance.data.statistics.Where(s => s.code == code).FirstOrDefault();
        if (stat == null) {
            return;
        }
        //incremento la estadistica con el valor de aumento solicitado
        stat.value += amount;
        //puedo recorrer todos los elementos de logros, con el statcode y que sus logros esten desbloqeados
        foreach (Achievement achievement in
                 DataManager.instance.data.achievement.Where(a => a.statCode == code && !a.unlocked).AsEnumerable()) {
            //recorro todos los logros cuyo styatDCode coincida co la estadistica incrementada y que esten aun bloqueados
            //si la estadistica es mayor o igual al valor requeriddo par adeslboquear el logro
            if (stat.value >= achievement.targetAmount) {
                //desbloqueamos el logro
                achievement.unlocked = true;
                //y avisamos del desbloqueo a los suscritos
                OnAchivementUnlock?.Invoke(achievement.name, achievement.imageName);
                //adicionalmente, pedimos al datamanager que guarde las estadisticas.
                DataManager.instance.Save();
            }
        }
    }
    #endregion
}
