﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//para acceder a elememntos UI
using UnityEngine.UI;

public class AchievementUI : MonoBehaviour {
    #region VARIABLES ############################

    public Image imageUI;
    private Text nameUI;
    private Animator animator;
    #endregion

    #region EVENTS #####################
    // Start is called before the first frame update
    void Start() {
        nameUI = GetComponentInChildren<Text>();
        animator = GetComponent<Animator>();
    }



    private void OnEnable() {
        AchievementManager.OnAchivementUnlock += SetAndShow;
    }

    private void OnDisable() {
        AchievementManager.OnAchivementUnlock -= SetAndShow;
    }
    #endregion
    #region METHODS ######################
    public void SetAndShow(string name, string imageName) {
        //modifica ek valor del sprite, recuperando la imagen de la carpeta de resources
        imageUI.sprite = Resources.Load<Sprite>(imageName);
        //cambiamos nombre a mostrar
        nameUI.text = name;
        //iniciamos la animacion
        animator.SetTrigger("Show");
    }
    #endregion
}
