﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//para recuperar elementos de interfaz
using UnityEngine.UI;
//para poder realizar busquedas en el data
using System.Linq;
public class AchievementDisplay : MonoBehaviour {
    #region VARIABLES######
    //string del logro a recuperar
    public string achievementName;
    //referencias a los elementos del UI
    public Image image;
    public Text name;
    public Text description;
    public Image shadow;
    #endregion

    #region EVENTS ###########

    void Start() {
        Achievement result = DataManager.instance.data.achievement.Where(a => a.name == achievementName).FirstOrDefault();
        if (result != null) {
            //vuelco la información del logro a los componentes del UI
            image.sprite = Resources.Load<Sprite>(result.imageName);
            name.text = result.name;
            description.text = result.description;
            shadow.enabled = !result.unlocked;
        }
    }
    #endregion
}
