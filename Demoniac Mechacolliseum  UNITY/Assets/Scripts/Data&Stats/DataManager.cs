﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//para poder serializar en bnarios
using System.Runtime.Serialization.Formatters.Binary;

using System.IO;
public class DataManager : MonoBehaviour {
    #region VARIABLES ############################
    //VARIABLE QUE CONTENDRÁ toda la informacion de estadistica y logrros
    public Data data;
    // nombre en el que almacenaremos la información en la ruta local
    public string fileName = "data.dat";
    //ruta + nombre de fichero
    private string dataPath;

    public static DataManager instance;
    //queremos que este singleton aparezca entre escenas

    #endregion

    #region EVENTS #####################
    private void Awake() {
        if (instance == null) {
            //le indicamos que la instancia no se destruirá entre escenas
            DontDestroyOnLoad(gameObject);

            instance = this;
        }
        //si se trata de una instancia distinta a la actual
        else if (instance != this) {
            //nos autodestruimos
            Destroy(gameObject);
        }

        dataPath = Application.persistentDataPath + "/" + fileName;

        //para poder localizar la carpeta de data facilmente
        Debug.Log(dataPath);
        //inrentamos recuperar la informacion
        Load();
    }

    // Start is called before the first frame update
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }
    #endregion
    #region METHODS ######################
    [ContextMenu("Save")]
    /// <summary>
    /// guarda la informacion en el disco
    /// </summary>
    public void Save() {
        //objeto utilizado para serializar7deserializar
        BinaryFormatter bf = new BinaryFormatter();
        //creamos / sobreesribimos el fichero con los datos
        FileStream file = File.Create(dataPath);
        //serializamos el contenido de nuestor objeto de datos
        bf.Serialize(file, data);
        //cerramos el stream una vez terminado
        file.Close();
    }
    /// <summary>
    /// Recupera la informacion guardada en el disco
    /// </summary>
    void Load() {
        if (!File.Exists(dataPath)) {
            return;
        }
        BinaryFormatter bf = new BinaryFormatter();

        FileStream file = File.Open(dataPath, FileMode.Open);
        //deserliazimaos el fchero utilizando la estructura de la clase Data
        data = (Data)bf.Deserialize(file);
        //una vez terminado, cerramos el fichero
        file.Close();


    }
    #endregion
}
