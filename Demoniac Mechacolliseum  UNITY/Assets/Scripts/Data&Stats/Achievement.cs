﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Achievement {
    public string name;
    //codigfo de la estadistica a verificar
    public string statCode;
    //nombre del sprite a cargar en  resources
    public string imageName;
    //descripcion del juego
    public string description;
    //cantidad a conseguir 
    public int targetAmount;
    //variable para indicar si el logro estça o no está deswblouqueado
    //variable para indicar si el logro estça o no está deswblouqueado
    public bool unlocked = false;
}
