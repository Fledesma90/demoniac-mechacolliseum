﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTrack : MonoBehaviour
{
    #region VARIABLES ############################
    //será el transform al que seguirá la cámara
    public Transform target;
    //desplazamiento en Z respecto a la posicion del target
    public float zOffset = 2f;
    //velocidad de transicion de la cámara a la nueva posición
    public float transitionSpeed = 1f;
    //layer q utilizaremos para identificar las colisiones de la cámara
    public LayerMask occlusionLayer;
    //variable para almacenar el resultado del impacto del raycast de oclusión
    private RaycastHit occlusionHit = new RaycastHit();
    //posicion temporal de la cámara
    private Vector3 tempPosition;
#endregion

#region EVENTS #####################
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    }
    //si ponemos el cameramoveandrotate en el fixed update NO HABRÁN TIRONES EN LA CÁMARA.
    private void FixedUpdate()
    {
        CameraMoveAndRotate();

    }
    #endregion
    #region METHODS ######################
    /// <summary>
    /// Posiciona y rota la cámara respecto al objetivo
    /// </summary>
    private void CameraMoveAndRotate()
    {
        tempPosition.Set(target.position.x,
                         transform.position.y,
                         target.position.z - zOffset);

        //verificamos si hay una oclusión y modificamos la posición de la cámara en consecuencia
        CheckOcclusion(ref tempPosition);

        //el transform punto position va a ser igual a tempopisition
        // transform.position = tempPosition;
        
        //suavizamos el desplazamiento de la camara
        transform.position = Vector3.Lerp(transform.position,
                                                tempPosition,
                                                 Time.deltaTime*transitionSpeed);

        //estamos haciendo suavizado del giro de la camara al mirar al objetivo.
        //para obtener un vector direccional le resto al ddestino el origen
        transform.rotation = Quaternion.Lerp(transform.rotation,
                                           Quaternion.LookRotation(target.position - transform.position),
                                           Time.deltaTime);
    }
    //variable de referencia a posicion
    /// <summary>
    /// Comprueba si hay un obstáculo entre target y cámara
    /// Modificará la posición recibida para esquivar el obstáculo
    /// </summary>
    /// <param name="pos"></param>
    private void CheckOcclusion(ref Vector3 pos)
    {
        //linea de debug para verificar de forma visual el linecast
        Debug.DrawLine(target.position, pos, Color.red);
        //lanzamos un linecast desde el target a la posicion de la cámara
        if (Physics.Linecast(target.position,
                            pos,
                            out occlusionHit,
                            occlusionLayer)){
            //en caso de impacto, cambiamos la posicion de la cámara a la posicion del impacto 
            //pero respetamos la posicion en el eje Y
            pos.Set(occlusionHit.point.x,
                pos.y,
                occlusionHit.point.z);
            //lanzamos un ray hacia arriba en el punto de impacto
            Debug.DrawRay(occlusionHit.point, Vector3.up, Color.green);
        }
    }
#endregion
}
