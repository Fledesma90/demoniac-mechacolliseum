﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickableItem : MonoBehaviour
{
    //con esta variable podemos coger los tipos de powerups que hemos mencionado en
    //el script player controller
    public PlayerController.powerUps type;
    public Sprite itemSprite;
    public Sprite frameSprite;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
