﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraBehaviour : MonoBehaviour
{
    public CinemachineVirtualCamera currentVC;

    private CinemachineBasicMultiChannelPerlin noise;

    private void Awake() {
        noise = currentVC.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
    }

    private IEnumerator Shake(float shakeTime) {
        //Cuanto se mueve la cámara
        noise.m_AmplitudeGain = 5;
        //Con qué frecuencia se mueve la cámara
        noise.m_FrequencyGain = 5;

        //esto es lo que va a durar la vibración antes de desactivarse
        yield return new WaitForSeconds(shakeTime);

        noise.m_AmplitudeGain = 0;
        noise.m_FrequencyGain = 0;
    }

    public void DoShake(float shakeTime) {

        StartCoroutine(Shake(shakeTime));

    }
}
