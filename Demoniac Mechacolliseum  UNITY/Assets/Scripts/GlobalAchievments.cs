﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GlobalAchievments : MonoBehaviour
{
    
    
    public bool achActive = false;
    
    //Achievement I<3U
    public GameObject panelAch1;
    
   
   
    public static int achIloveuCounter;
    public int achIloveuTrigger = 5;
    public int achIloveuCode;


    private bool achievementShown;
    private bool achievementShownOne;
    private bool achievementShownTwo;
    private bool achievementShownThree;
    private bool achievementShownFour;
    private bool achievementShownFive;
    private bool achievementShownSix;
    private bool achievementShownSeven;
    //Achievement 2

    public GameObject panelAch2;

    //Achievement 3

    public GameObject panelAch3;

    //Achievement 4

    public GameObject panelAch4;

    //Achievement 5

    public GameObject panelAch5;

    //Achievement 6

    public GameObject panelAch6;

    //Achievement 7

    public GameObject panelAch7;
    public static int shieldCounter;
    public int shieldTrigger = 1;
    public int shieldCode;

    //Achievement 8

    public GameObject panelAch8;
    public static int mgCounter;
    public int mgCode;
    public int mgTrigger = 1;


    // Update is called once per frame
    void Update()
    {
        achIloveuCode = PlayerPrefs.GetInt("Ach01");
        if(achIloveuCounter==achIloveuTrigger && achIloveuCode != 1 && achievementShownOne==false) {
            achievementShownOne = true;
            StartCoroutine(TriggerIloveu());
        }
        OpenAch2Panel();
        OpenAch3Panel();
        OpenAch4Panel();
        OpenAch5Panel();
        OpenAch6Panel();
        OpenAch7Panel();

    }

    public void OpenAch2Panel() {
        if (GameManager.instance.currentWave==2 && achievementShown==false) {
            achievementShown = true;
           StartCoroutine(FirstWave());
        }

    }

    public void OpenAch3Panel() {
        if (GameManager.instance.currentWave == 4 && achievementShownTwo==false) {
            achievementShownTwo = true;
            StartCoroutine(ThreeWaves());
        }
    }

    public void OpenAch4Panel() {
        if (GameManager.instance.currentWave == 6 && achievementShownThree==false) {
            achievementShownThree = true;
            StartCoroutine(FiveWaves());
        }
    }

    public void OpenAch5Panel() {
        if (GameManager.instance.currentWave == 11&& achievementShownFour==false) {
            achievementShownFour = true;
            StartCoroutine(TenWaves());
        }
    }

    public void OpenAch6Panel() {
        if (GameManager.instance.currentWave == 21&& achievementShownFive==false) {
            achievementShownFive = true;
            StartCoroutine(TwentyWaves());
        }
    }
    public void OpenAch7Panel() {
        shieldCode = PlayerPrefs.GetInt("Ach07");
        if (shieldCounter == achIloveuTrigger && shieldCode != 1 && achievementShownSix==false) {
            achievementShownSix = true;
            StartCoroutine(TriggerShield());
        }
    }

    public void OpenAch8Panel() {
        mgCode = PlayerPrefs.GetInt("Ach08");
        if (mgCounter == achIloveuTrigger && mgCode != 1 && achievementShownSeven==false) {
            achievementShownSeven = true;
            StartCoroutine(TriggerMachineGun());
        }
    }
    IEnumerator FirstWave() {
        achActive = true;
        panelAch2.SetActive(true);
        yield return new WaitForSeconds(5);
        panelAch2.SetActive(false);
        achActive = false;
        
    }

    IEnumerator ThreeWaves() {
        achActive = true;
        panelAch3.SetActive(true);
        yield return new WaitForSeconds(5);
        panelAch3.SetActive(false);
        achActive = false;
    }

    IEnumerator FiveWaves() {
        achActive = true;
        panelAch4.SetActive(true);
        yield return new WaitForSeconds(5);
        panelAch4.SetActive(false);
        achActive = false;
    }

    IEnumerator TenWaves() {
        achActive = true;
        panelAch4.SetActive(true);
        yield return new WaitForSeconds(5);
        panelAch4.SetActive(false);
        achActive = false;
    }
    IEnumerator TwentyWaves() {
        achActive = true;
        panelAch4.SetActive(true);
        yield return new WaitForSeconds(5);
        panelAch4.SetActive(false);
        achActive = false;
    }
    IEnumerator TriggerIloveu() {
        achActive = true;
        panelAch1.SetActive(true);
        achIloveuCode = 1;
        PlayerPrefs.SetInt("Ach01", achIloveuCode);
        yield return new WaitForSeconds(5);
        //Resetting UI
        panelAch1.SetActive(false);
      
        achActive = false;
    }

    IEnumerator TriggerShield() {
        achActive = true;
        panelAch7.SetActive(true);
        shieldCode = 1;
        PlayerPrefs.SetInt("Ach07", shieldCode);
        yield return new WaitForSeconds(5);
        //Resetting UI
        panelAch7.SetActive(false);

        achActive = false;
    }

    IEnumerator TriggerMachineGun() {
        achActive = true;
        panelAch8.SetActive(true);
        mgCode = 1;
        PlayerPrefs.SetInt("Ach08", mgCode);
        yield return new WaitForSeconds(5);
        //Resetting UI
        panelAch8.SetActive(false);

        achActive = false;
    }
}
