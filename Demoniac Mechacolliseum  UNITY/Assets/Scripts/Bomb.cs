﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    //variable para retrasar la explosión
    public float delay = 3f;
    //variable para indicar el radio de la explosión
    public float radius = 5f;

    public float force = 700f;

    public float damage = 40f;

    //referencia para las partículas
    public GameObject explosionEffect;


    private float countdown;
    private bool hasExploded=false;
    // Start is called before the first frame update
    void Start()
    {
        countdown = delay;
    }

    // Update is called once per frame
    void Update()
    {
        countdown -= Time.deltaTime;
        if (countdown <= 0f && !hasExploded) {
            Explode();
            hasExploded = true;
        }
    }

    void Explode() {

//        FindObjectOfType<CameraBehaviour>().DoShake(0.2f);

        Instantiate(explosionEffect, transform.position, transform.rotation);
        //objetos que colisionarán con la explosión
        Collider[] colliders=Physics.OverlapSphere(transform.position, radius);


        foreach(Collider nearbyobject in colliders) {
            Rigidbody rb = nearbyobject.GetComponent<Rigidbody>();
            if (rb != null) {
                rb.AddExplosionForce(force, transform.position, radius);
            }
            if (nearbyobject.GetComponent<Enemy>() !=null) {
                nearbyobject.GetComponent<Enemy>().TakeHealth(damage);
            }
        }
        

        //se destruye la bomba tras explotar
        Destroy(gameObject);

        
    }
}
