﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(CapsuleCollider))]
[RequireComponent(typeof(SphereCollider))]

public class PlayerController : MonoBehaviour
{
    //creamos una variable para indicar la velocidad
    private float velocity;
    //creamos una variable para almacenar el RigidBody del Player y así poder cambiar la velocity y se mueva el jugador
    public Rigidbody RB;

    public GameObject bulletPrefab;

    public Transform gun;

    public bool inDash;
    public float dashVelocity;
    public bool canDash=true;
    public float normalVelocity;
    private float shootTimer;
    public float startShootTimer;
    public bool machineGunOn=false;
    private int ammo;
    public int ammoMax;
    public float velocityMachinegun;
    public bool deadPlayer;
    public float lives;
    private float maxLives;
    public bool bomb;
    //fuerza con la que tiramos la bomba
    public float throwForce = 40f;
    public GameObject bombPrefab;
    public GameObject shieldObj;
    public bool withShield;
    public enum powerUps { bomb, shield, nothing};
    public powerUps currentPowerUp = powerUps.bomb;

    public Image uiItem;
    public Image uiItemFrame;
    public Sprite emptyImage;
    //tiempo que dura el cooldown
    public float startCooldown;
    //variable para contar el cooldown
    private float cooldownCounter;

    public Transform cameraTarget;

    public float cameraFollowOffset;

    private Animator anim;

    public Image healthUI;
    
    public GameObject PowerUpsPrefabLives;
    public GameObject PowerUpsPrefabsShield;
    public GameObject PowerUpsPrefabsMg;
    public GameObject PowerUpsPrefabsBomb;

    public ParticleSystem deathPlayerParticles;

    public bool playerDeadAnim;

    private CapsuleCollider capsuleCollider;
    private SphereCollider sphereCollider;
    public GameObject playerModel;
    public GameObject mgImageUi;

    public AudioClip shootSound;

    public bool shieldToggle;

    // Start is called before the first frame update
    void Start()
    {
        velocity = normalVelocity;
        maxLives = lives;
        anim = GetComponent<Animator>();
        capsuleCollider = GetComponent<CapsuleCollider>();
        sphereCollider = GetComponent<SphereCollider>();


    }

    // Update is called once per frame
    void Update()
    {
        Aim();
        Controls();
        if (shootTimer > 0) {
            //ponemos un cooldown al disparo
            shootTimer -= Time.deltaTime;
        }
        if (cooldownCounter>0) {
            cooldownCounter -= Time.deltaTime;
            if (uiItem.fillAmount<1) {
                uiItem.fillAmount += 1f / cooldownCounter * Time.deltaTime;
            }
        }
        cameraTarget.position = new Vector3(transform.position.x, transform.position.y, transform.position.z - cameraFollowOffset);
        healthUI.fillAmount = lives / maxLives;
    }
    //ponemos un FixedUpdate para gestionar los campos de físicas
    private void FixedUpdate() {
        Movement();
        
    }

    

    void Movement() {
        float inputH = Input.GetAxisRaw("Horizontal");
        float inputV = Input.GetAxisRaw("Vertical");
        RB.velocity = new Vector3(inputH * velocity * Time.deltaTime, RB.velocity.y, inputV * velocity * Time.deltaTime);
    }

    void Aim() {
        if (!deadPlayer) {
            Ray CameraRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            Plane groundplane = new Plane(Vector3.up, Vector3.zero);
            float raylenght;
            if (groundplane.Raycast(CameraRay, out raylenght)) {
                Vector3 pointolook = CameraRay.GetPoint(raylenght);
                //lanzamos un rayo desde la cámara miramos donde está chocando y definimos que ese es el punto donde va a mirar el jugador.
                //Ponemos una linea para visualizar que funciona el movimiento del ratón o rayo que lanzamos desde la cámara.
                Debug.DrawLine(CameraRay.origin, pointolook, Color.blue);

                transform.LookAt(new Vector3(pointolook.x, transform.position.y, pointolook.z));
            }
            // Vector3 MousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //float Angle = Mathf.Atan2(LookDir.y, LookDir.x) * Mathf.Rad2Deg - 90f;
            //Vector2 LookDir = MousePosition - new Vector2 (transform.position.x,transform.position.z);
            //transform.eulerAngles = Angle;
        }
    }

    void Shoot() {
        if (shootTimer <= 0 && inDash==false) {
            //comprobamos si tenemos el powerup recogido y detectamos si el juego tiene que hacer disparo normal
            //ó de metralleta
            if (machineGunOn==false) {
                Instantiate(bulletPrefab, gun.position, transform.rotation);
                shootTimer = startShootTimer;
            } else {
                GlobalAchievments.mgCounter += 1;
                //establecemos un menor tiempo entre balas en este caso 1/3
                shootTimer = velocityMachinegun;
                Instantiate(bulletPrefab, gun.position, transform.rotation);
                //le quitamos uno a las balas cada vez que disparemos
                ammo--;
                if (ammo<=0) {
                    machineGunOn = false;
                    mgImageUi.SetActive(false);
                }
            }
            anim.SetTrigger("Shoot");
            SfxAudio.instance.PlaySound(shootSound);
           
        }
    }

    void Controls() {
        if (!deadPlayer) {

            if (Input.GetMouseButton(0)) {
                Shoot();
            }

            if (Input.GetButtonDown("Dash") && canDash==true) {
                StartCoroutine(Dash());
            }
            if (Input.GetButtonDown("Fire2") && cooldownCounter<=0) {
                UseItems();
            }

        }

    }
    //comprobamos que el objeto con el que chocamos tiene el script machinegunPowerup
    private void OnTriggerEnter(Collider other) {
        if (other.GetComponent<machinegunPowerup>()!=null) {
            machineGunOn = true;
            Destroy(other.gameObject);
            //se rellena la munición
            ammo = ammoMax;
            mgImageUi.SetActive(true);
            
        }
        if (other.GetComponent<PickableItem>() != null) {
            PickItem(other.gameObject);
        }

        if (other.GetComponent<LivesPowerUp>()!=null) {
            AddLives(other.GetComponent<LivesPowerUp>().livesToAdd);
            other.GetComponent<LivesPowerUp>().TakeLives();
            
        }
        if (other.CompareTag("Shield")) {
            GlobalAchievments.shieldCounter += 1;
            shieldToggle = true;
        }

        if (other.CompareTag("MachineGun")) {
            GlobalAchievments.mgCounter += 1;
        }

    }



    IEnumerator Dash() {
        velocity = dashVelocity;
        inDash = true;
        canDash = false;
        yield return new WaitForSeconds(0.2f);
        velocity = normalVelocity;
        inDash = false;
        yield return new WaitForSeconds(0.3f);
        canDash = true;
    }

    void ThrowBomb() {
        GameObject Bomb = Instantiate(bombPrefab, transform.position, transform.rotation);
        Rigidbody rb = Bomb.GetComponent<Rigidbody>();
        rb.AddForce(transform.forward * throwForce, ForceMode.VelocityChange);
        cooldownCounter = startCooldown;
        uiItem.fillAmount = 0;
        anim.SetTrigger("Shoot");
    }

    void ActivateShield() {
        if (!shieldToggle) {
            return;
        }
        withShield = true;
        shieldObj.SetActive(true);
        cooldownCounter = startCooldown;
        uiItem.fillAmount = 0;
        shieldToggle = false;
    }

    
    
    public void TakeHealth(float livesToTake) {
        if (inDash==false && withShield==false) {
            lives -= livesToTake;

        }

        if (withShield) {
            withShield = false;
            shieldObj.SetActive(false);
        }

        
        if (lives <= 0) {
            deadPlayer = true;
            Instantiate(deathPlayerParticles, transform.position, Quaternion.identity);
            Invoke("OpenDeathMenu", 2f);
            Destroy(playerModel);
            capsuleCollider.enabled = false;
            sphereCollider.enabled = false;
            anim.SetTrigger("Prueba");
            
           //Meter animacion de morir
           //Menu de muerte
        }
    }

    public void OpenDeathMenu() {
        
        GameManager.instance.GameOver();
        Time.timeScale = 0f;
    }


    void PickItem(GameObject item) {
        PickableItem itemscr = item.GetComponent<PickableItem>();
        currentPowerUp = itemscr.type;
        cooldownCounter = 0;
        uiItem.sprite = itemscr.itemSprite;
        uiItemFrame.sprite = itemscr.frameSprite;

        uiItem.fillAmount = 1;
        Destroy(item);
    }

    private void UseItems() {
        switch (currentPowerUp) {
            case powerUps.bomb:
                ThrowBomb();
                break;
            case powerUps.shield:
                ActivateShield();
                break;
            default:
                break;
        }
        
    }
    //creamos el powerup de vidas para que el máximo sea el float lives
    private void AddLives(float LivesToAdd) {
       
        if (lives+LivesToAdd<=100) {
            lives += LivesToAdd;
        } else {
            lives = maxLives;
        }
    }

    public void InstantiatePowerUpsLives() {
        
        Instantiate(PowerUpsPrefabLives, gun.position, transform.rotation);
        
    }
    public void InstantiatePowerUpsShield() {
        Instantiate(PowerUpsPrefabsShield, gun.position, transform.rotation);
        
    }
    public void InstantiatePowerUpsMg() {
        Instantiate(PowerUpsPrefabsMg, gun.position, transform.rotation);
        
    }
    public void InstantiatePowerUpsBomb() {
        Instantiate(PowerUpsPrefabsBomb, gun.position, transform.rotation);
        
    }


}
