﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SfxAudio : MonoBehaviour
{
    public AudioSource audioSource;
    public static SfxAudio instance;

    private void Awake() {
        if (instance == null) {
            instance = this;
        }
    }

    public void PlaySound(AudioClip sfx) {
        audioSource.PlayOneShot(sfx);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
