﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class SceneController : MonoBehaviour
{
    /// <summary>
    /// Cmmbia a la escena recibida como parámetro
    /// </summary>
    /// <param name="sceneName"></param>
    #region METHODS
    public void ChangeScene(string sceneName) {
        Time.timeScale = 1f;

        SceneManager.LoadScene(sceneName);
    }
    //Destruimos los objetos con el tag GameMusic cuando entramos en el juego
    public void DestroyMusic() {
        GameObject[] musicObjects = GameObject.FindGameObjectsWithTag("GameMusic");
        for (int i = 0; i < musicObjects.Length; i++) {
            Destroy(musicObjects[i]);

        }
    }
    public void ExitGame() {
        Application.Quit();
    }
#endregion
}
