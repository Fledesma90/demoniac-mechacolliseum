﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpCheats : MonoBehaviour
{

    public float delay = 10f;

    public float force = 700f;

    private float countdown;

    public bool hasDisappeared;

    // Start is called before the first frame update
    void Start()
    {
        countdown = delay;
    }

    // Update is called once per frame
    void Update()
    {
        countdown -= Time.deltaTime;
        if (countdown<=0 && !hasDisappeared) {
            Destroy(gameObject);
        }
    }
}
