﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

using UnityEngine.UI;

using TMPro;

public class GameManager : MonoBehaviour
{

    [Header("End Game Menu")]
    //referencia al HUD durante la partida
    public GameObject inGameHUD;
    // referencia al menu de Game Over
    public GameObject deadMenu;
    

    [Header("Paused Menu")]
    //referencia al panel con el menú de pausa
    public GameObject pauseMenu;
    //booleana para identificar si esta pausado el juego
    public bool paused = false;

    [Header("Cheat Menu")]

    //referencia al panel con el menu de trucos
    public GameObject cheatMenu;

    [Header("HUD")]
    //Animator del hud de oleadas
    //public Animator waveAnimator;
    //texto del numero de oleadas
    //public Text waveNumber;

    [Header("Enemies")]
    //prefabs de enemigos
    public GameObject[] enemiesPrefab;
    //prefabs de bosses
    public GameObject[] enemiesBossPrefab;
    //puntos de spawn de los enemigos
    public Transform[] spawnPoints;
    //transform padre de los enemigos
    public Transform enemiesParent;

    [Header("Waves")]

    public float spawnDelay = 0.2f;

    public int waveEnemyNumberMultiplier = 20;

    public int waveBoss = 3;

    private int waveEnemies;

    private int remainingEnemies;

    public int currentWave = 0;

    private float spawnTimer = 0f;

    public int maxEnemiesOnScene = 5;
    private int enemiesOnScene;


    public static GameManager instance;

    public TextMeshProUGUI RoundText;

    public GameObject volumePanel;

    private void Awake() {
        //aseguro instancia unica de singleton
        if (instance == null) {
            instance = this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        //nada mas comenzar partida iniciamos la primera oleada
        NewWave();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Cancel")) {
            Pause(!paused);
        }
        if (spawnTimer <= spawnDelay) {
            spawnTimer += Time.deltaTime;
        }

        if (waveEnemies > 0 && spawnTimer > spawnDelay && enemiesOnScene < maxEnemiesOnScene) {
            //generamos nuevo enemigo
            GenerateEnemy(enemiesPrefab);
            //reseteamos contador
            spawnTimer = 0;
        }
        //si han muerto todos los enemigos
        if (remainingEnemies <= 0) {
            //genero una nueva oleada
            NewWave();
            
        }
        //si se mantiene pulsada la tecla ctrl y se pulsa una sola vez la Q alternamos la apertura del menú de trucos.
        if (Input.GetKeyDown(KeyCode.Q) && Input.GetKey(KeyCode.LeftControl)) {
            //toggle del menú de trucos
            cheatMenu.SetActive(!cheatMenu.activeSelf);
        }
    }

    public void GameOver() {
        
        // mostraremos el menu de fin de partida
        deadMenu.SetActive(true);
        
    }

    /// <summary>
    /// Realiza las acciones necesarias para pausar o renaudar la partida
    /// </summary>
    /// <param name="pause"></param>
    public void Pause(bool pause) {
        // si el parametro recibido es true
        if (pause) {
            // mostramos el menú
            pauseMenu.SetActive(true);
            //paramos la escala de tiempo
            Time.timeScale = 0f;
            //indicamos que el juego está pausado
            paused = true;

        } else {
            //ocultamos el menu
            pauseMenu.SetActive(false);
            //pongo la escala de tiempo al valor normal
            Time.timeScale = 1f;
            //indicamos que el juego no esta pausado
            paused = false;
        }
    }

    /// <summary>
    /// Genera un enemigo aleatorio a partir de la lista recibida 
    /// </summary>
    /// <param name="enemyPool"></param>
    public void GenerateEnemy(GameObject[] enemyPool) {
        if (spawnPoints.Length == 0 || enemyPool.Length == 0) {
            //nos dejamos a nosotros un comentario
            Debug.LogError("te has olvidado de configurar o los spawns o los enemigos ");
            return;
        }

        //obtengo indices aleatorios de los arrays
        int randomSpawn = Random.Range(0, spawnPoints.Length);
        int randomEnemy = Random.Range(0, enemyPool.Length);

        //Ponemos que la primera oleada aparezcan los enemigos fáciles
        if (currentWave == 1) {
            GameObject tempEnemy = Instantiate(enemyPool[0],
                                spawnPoints[randomSpawn].position,
                                Quaternion.identity,
                                enemiesParent);
        } else {
            Instantiate(enemyPool[randomEnemy],
                                    spawnPoints[randomSpawn].position,
                                    Quaternion.identity,
                                    enemiesParent);
        }
        //tras generar un nuevo enemigo, decremento el contador de enemigos a generar
        waveEnemies--;
    }

    /// <summary>
    /// Para indicar que un enemigo ha muerto
    /// </summary>
    public void EnemyDead() {
        //reducimos el numero de enemigos restantes de la oleada
        remainingEnemies--;
        //decrementamos el numero de enemigos en la escena
        enemiesOnScene--;
    }

    /// <summary>
    /// Realizamos las gestiones necesarias para generar una nueva oleada
    /// </summary>
    public void NewWave() {

        Debug.Log("creamos una nueva oleada");
        //incrementamos el numero de oleada actual
        currentWave++;
        RoundText.text = "Round " + currentWave;

        //waveNumber.text = currentWave.ToString();

        //waveAnimator.SetTrigger("Show");

        waveEnemies = currentWave * waveEnemyNumberMultiplier;

        remainingEnemies = waveEnemies;
        enemiesOnScene = 0;

        if (currentWave % waveBoss == 0) {
            for (int i = 0; i < currentWave/waveBoss; i++) {
                GenerateEnemy(enemiesBossPrefab);
            }
        }

    }
    /// <summary>
    /// Reinicia el nivel
    /// </summary>
    public void RestartLevel() {
        //restauramos el timescale antes de reiniciar el nivel
        Time.timeScale = 1f;
        //recargamos la esena
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void VolumePanel() {
        if (volumePanel != null) {
            bool isActive = volumePanel.activeSelf;

            volumePanel.SetActive(!isActive);
        }
    }
}
