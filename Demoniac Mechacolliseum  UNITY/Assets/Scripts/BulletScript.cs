﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{

    public float velocity;

    public Rigidbody RB;

    public float damage;

    public bool playerBullet;

    public AudioClip[] voices;

    // Start is called before the first frame update
    void Start()
    {
        RB.velocity = transform.forward * velocity;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other) {
        //comprobamos si la bala es del player para que no se dañen los enemigos cuando disparen
        if (other.GetComponent<Enemy>()!=null && playerBullet) {
            //si la bala que lo mata le causa un daño que sea menor o igual que 0 llamamos a la funcion PLAYVOICES
            if (other.GetComponent<Enemy>().health-damage<=0) {
                PlayVoices();
            }
            //queremos coger el componente de other que es el objeto con el
            //que estamos chocando
            other.GetComponent<Enemy>().TakeHealth(damage);
            Destroy(gameObject);
        }
        //Aquí el jugador recibe daño
        if(other.CompareTag("Player") && !playerBullet){
            other.GetComponent<PlayerController>().TakeHealth(damage);
            Destroy(gameObject);
        }
        if (other.CompareTag("Wall")) {
            Destroy(gameObject);
        }
    }

    void PlayVoices() {
        if (Random.Range(0,100)<=25) {
            SfxAudio.instance.PlaySound(voices[Random.Range(0, voices.Length)]);
        }
    }

}
